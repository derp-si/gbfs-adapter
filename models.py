from typing import override
from pydantic import BaseModel

class CustomBaseModel(BaseModel):

    @override
    def model_dump(self, *args, **kwargs):
        kwargs['exclude_none'] = True
        return super().model_dump(*args, **kwargs)
