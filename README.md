# GBFS adapters for all kinds of providers

## Supported providers

 - Avant2Go (`avant2go`)
 - SHARE'Ngo (`sharengo`)
 - Smart City Bikes (`scbikes_*`)

## Configuration

Configuration is done through environment variables:

 - `ENVIRONMENT` (required) - two-part value: `{dev,prod,test}-{name}` (e.g. `dev-local`, `prod-primary`, `test-gitlabci`...). Many options depend on this (see `config.py`) and it's used for error reporting. 
 - `SENTRY_DSN` (optional) - if given (and not in a `dev` environment), will report errors to Sentry on the given URL
 - `PROVIDERS` (optional) - a comma-separated list of providers to enable (defaults to all of them, see above for a list of names)

## Deployment

Prebuilt Docker images are available in the project's [GitLab container registry](https://gitlab.com/derp-si/gbfs-adapter/container_registry).  

```bash
docker run --name gbfs-adapter -e BASE_URL="http://localhost" registry.gitlab.com/derp-si/gbfs-adapter:v0.1 
```

## For developers

### Local setup

 1. Clone this repository with `--recurse-submodules` or sync the submodule manually
 2. Install dependencies with `pipenv install` 
 3. Run the `build_schema.sh` script to generate the GBFS bindings
 4. Set the required environment variables and run `main.py`

### Adding a new provider

 1. Create a new file in `providers/` and add its name to `PROVIDER_NAMES` in `main.py`
 2. In that file, create a class called `Provider` that extends `GBFSProvider`
 3. Implement the methods the parent class requires


### Running tests

 1. Run the validator service:
```sh
docker run --rm --name=gbfs-validator --net=host ghcr.io/franga2000/gbfs-validator:test5
```

 2. Run the develpment server however you usually do
 3. Run the tests using your IDE or `./run_tests.sh`
 4. 
## License

This project is licensed under the GNU Affero General Public License.
