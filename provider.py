from gbfs_model import system_information
from gbfs_model.free_bike_status import Bike
from gbfs_model.system_information import Timezone
from gbfs_model.vehicle_types import VehicleType


class GBFSProvider:
    name: str
    website: str
    id: str
    supported_feeds = ('system_information',)
    language = 'en'
    timezone = Timezone.Europe_Berlin

    @classmethod
    def create_providers(cls):
        return [cls()]

    async def setup(self):
        pass

    def get_url(self, base_url='', path='gbfs.json'):
        return base_url + f'/gbfs/{self.id}/{path}'

    def get_system_info(self):
        return system_information.Data(
            system_id=self.id,
            language=self.language,
            name=self.name,
            timezone=self.timezone,
        )

    async def get_free_bike_status(self) -> list[Bike]:
        return []

    async def get_vehicle_types(self) -> list[VehicleType]:
        return []
