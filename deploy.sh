#!/bin/sh
set -e

export DOCKER_HOST="ssh://arm1.frangez.me"

docker build . -t registry.gitlab.com/derp-si/gbfs-adapter:dev

docker stop gbfs-adapter || true
docker rm gbfs-adapter || true

docker run -d \
	--name=gbfs-adapter \
	--restart=always \
	-p 127.0.0.1:8015:80 \
	-e ENVIRONMENT=prod-arm1 \
	-e BASE_URL=https://gbfs.derp.si \
	-e SENTRY_DSN=https://4038006369e4e98835df3b2a14a9551b@o1099481.ingest.us.sentry.io/4507123483475968 \
	registry.gitlab.com/derp-si/gbfs-adapter:dev
