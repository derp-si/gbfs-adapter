import asyncio
from collections import defaultdict
from datetime import timedelta
import importlib
import logging
import time
from types import TracebackType

from fastapi import FastAPI
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend
from prometheus_fastapi_instrumentator import Instrumentator
from starlette.requests import Request

import config
import gbfs_router
from util import DummyCacheBackend

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("rest_api")
logger.setLevel(logging.DEBUG)

app = FastAPI(
    title="GBFS Adapters",
    description="GBFS adapters for a bunch of different vehicle sharing services"
)

app.include_router(gbfs_router.router)

#######################
#   SETUP PROVIDERS   #
#######################

for name in config.PROVIDER_NAMES:
    try:
        site = importlib.import_module('providers.' + name)
        for provider in site.Provider.create_providers():
            config.PROVIDER_MAP[provider.id] = provider
    except ImportError:
        import traceback
        traceback.print_exc()


@app.on_event("startup")
async def on_startup():
    if config.USE_CACHE:
        backend = InMemoryBackend()
    else:
        backend = DummyCacheBackend()
    FastAPICache.init(backend)

    for provider in config.PROVIDER_MAP.values():
        await provider.setup()

    print()
    print("Setup done!")
    print("Loaded", len(config.PROVIDER_MAP), "providers:")
    print('\t', ','.join([p.id for p in config.PROVIDER_MAP.values()]))
    print()



##############
#   SENTRY   #
##############

if config.USE_SENTRY:
    # If an error happens on the same line, ignore it for this amount of time (so we don't burn through our quota)
    SENTRY_MAX_PER_LINE = timedelta(hours=10).total_seconds()
    last_sent = defaultdict(float)

    def sentry_filter(event, hint):
        if 'exc_info' in hint:
            exc_type, exc_value, tb = hint['exc_info']
            tb: TracebackType
            fingerprint = f'{tb.tb_frame.f_lineno}:{tb.tb_frame.f_code.co_filename}'
            if time.time() - last_sent[fingerprint] > SENTRY_MAX_PER_LINE:
                last_sent[fingerprint] = time.time()
                return event
            return None
        return event

    logger.info("Connecting to Sentry")
    import sentry_sdk
    sentry_sdk.init(
        dsn=config.SENTRY_DSN,
        before_send=sentry_filter,
        traces_sample_rate=0,
    )


###############
#   METRICS   #
###############

instrumentator = Instrumentator().instrument(app)
instrumentator.expose(app, include_in_schema=False)

######################
#   META ENDPOINTS   #
######################

@app.get('/providers')
async def list_providers(request: Request):
    return [{
        'id': p.id,
        'name': p.name,
        'url': p.get_url(base_url=request.url.scheme + '://' + request.url.netloc),
    } for p in config.PROVIDER_MAP.values()]


@app.get('/discovery')
async def discovery(request: Request):
    return {
        "systems": [{
            'id': p.id,
            'url': p.get_url(base_url=request.url.scheme + '://' + request.url.netloc),
        } for p in config.PROVIDER_MAP.values()]
    }

###############
#   STARTUP   #
###############

if __name__ == '__main__':
    from hypercorn.asyncio import serve
    import hypercorn
    from hypercorn.middleware.proxy_fix import ProxyFixMiddleware

    cfg = hypercorn.config.Config()
    cfg.bind = '0.0.0.0:' + str(config.PORT)
    cfg.loglevel = 'info'
    # log every request
    cfg.accesslog = '-'

    # fixed_app = ProxyFixMiddleware(app, mode="legacy", trusted_hops=1)
    asyncio.run(serve(app, cfg))