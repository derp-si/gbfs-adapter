FROM python:3.12-slim

WORKDIR /usr/src/app
ENV PORT=80
EXPOSE ${PORT}

# Required to fix the data model
RUN apt-get update && apt-get install -y jq

# PIP_NO_CACHE_DIR=off mean NO CACHING. This is stupid, but it's the way it is. See https://github.com/pypa/pip/issues/2897#issuecomment-115319916
RUN PIP_NO_CACHE_DIR=off pip install pipenv

ENV ENVIRONMENT=prod-docker

# Copy Pipfile separately for build caching
COPY Pipfile Pipfile.lock ./
RUN PIP_NO_CACHE_DIR=off pipenv install --system --deploy

COPY gbfs-json-schema gbfs-json-schema
COPY build_schema.* ./
RUN sh ./build_schema.sh

# Copy the source
COPY . .

CMD python main.py
