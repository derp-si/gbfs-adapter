import time
from collections import defaultdict

import aiohttp as aiohttp

from gbfs_model.station_information import Station
from gbfs_model.station_status import Station as Station1, VehicleTypesAvailableItem, VehicleDocksAvailableItem
from gbfs_model.vehicle_types import VehicleType, FormFactor, PropulsionType
from provider import GBFSProvider


PROVIDERS = {
    "SI": "58ee0cc36d818563a9ff46af",
    "HR": "590c3e989bb8c5519abaa93e",
}


class Provider(GBFSProvider):
    id = "avant2go"
    name = "Avant2Go"
    supported_feeds = ('system_information', 'station_information', 'station_status', 'vehicle_types', )

    avant_system: str

    @classmethod
    def create_providers(cls):
        return [cls(system) for system in PROVIDERS.keys()]

    def __init__(self, system):
        self.avant_system = system
        self.name = f"Avant2Go {system}"
        self.id = f"avant2go_{system.lower()}"
        self.provider_id = PROVIDERS[system]

    async def _fetch(self, url, params=None):
        async with aiohttp.request("GET", url, params=params) as resp:
            resp.raise_for_status()
            data = await resp.json()
        return data

    async def _fetch_locations(self):
        url = "https://api.avant2go.com/api/locations"
        params = {
            "pupulate": '["companyID"]',
            "limit": 1000,
            "offset": 0,
            "reservable": "true",
            "providerID": PROVIDERS[self.avant_system],
        }
        return (await self._fetch(url, params))['results']

    async def _fetch_models(self):
        url = "https://api.avant2go.com/api/carModels"
        return await self._fetch(url)

    async def _fetch_cars(self):
        url = "https://api.avant2go.com/api/cars"
        return await self._fetch(url)

    async def get_vehicle_types(self):
        models = await self._fetch_models()
        types = []

        for model in models:
            types.append(VehicleType(
                vehicle_type_id=model['_id'],
                form_factor=FormFactor.car,
                propulsion_type=PropulsionType.electric,
                max_range_meters=model['range'] * 1000,
                name=model['name'],
                make=model['manufacturer'],
                vehicle_image=model['mainImageResource']['href'],
            ))
        return types

    async def get_stations(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station(
                station_id=str(location['_id']),
                name=location['name'],
                lat=location['geoLocation']['lat'],
                lon=location['geoLocation']['lng'],
                address=location['address']['address1'] + ', ' + location['address']['city'],
                capacity=location['freeParkingPlaces'] + location['reservableCars'],
                is_charging_station='⚡' in location['name'],
            ))
        return stations

    async def get_station_status(self):
        locations = await self._fetch_locations()
        cars = await self._fetch_cars()

        model_ids = {car['carModelID'] for car in cars}
        station_model_cars = defaultdict(lambda: defaultdict(list))
        for car in cars:
            station_model_cars[car['locationID']][car['carModelID']].append(car)

        stations = []
        for location in locations:
            stations.append(Station1(
                station_id=str(location['_id']),
                num_bikes_available=location['reservableCars'],
                num_docks_available=location['freeParkingPlaces'],
                vehicle_types_available=[
                    VehicleTypesAvailableItem(
                        vehicle_type_id=model_id,
                        count=len([car for car in station_model_cars[location['_id']][model_id] if car['status'] == 'Free']),
                    )
                    for model_id in model_ids
                ],
                # This is not required by the standard, but OTP doesn't work without it
                vehicle_docks_available=[
                    VehicleDocksAvailableItem(
                        vehicle_type_ids=list(model_ids),
                        count=location['freeParkingPlaces'],
                    )
                ],
                is_installed=True,
                is_renting=True,
                is_returning=True,
                num_docks_disabled=0,  # avant2go doesn't disable docks
                last_reported=int(time.time()),  # TODO:avant2go: last report time
            ))
        return stations
