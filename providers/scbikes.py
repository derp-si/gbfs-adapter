import json
import time
import urllib.request

import aiohttp as aiohttp

from gbfs_model.station_information import Station
from gbfs_model.station_status import Station as Station1, VehicleTypesAvailableItem
from gbfs_model.vehicle_types import VehicleType, FormFactor, PropulsionType
from provider import GBFSProvider


class Provider(GBFSProvider):
    id = "scbikes"
    name = "SCBikes"
    supported_feeds = ('system_information', 'station_information', 'station_status', 'vehicle_types',)

    domain = None

    def __init__(self, system_info):
        self.system_info = system_info
        self.id = 'scbikes_' + system_info['domain'].split('//', maxsplit=1)[1].split('.', maxsplit=1)[0]
        self.name = system_info['name']

    @classmethod
    def create_providers(cls):
        obj = urllib.request.urlopen('https://mobile.scbikes.com/api/operators')
        jsonobj = obj.read().decode('utf-8')
        data = json.loads(jsonobj)
        return [cls(system) for system in data if system['id'] < 1000]

    async def _fetch_locations(self):
        url = self.system_info['domain'] + "/api/stations/public-list?format=json"
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()
        return data

    async def get_vehicle_types(self):
        types = [VehicleType(
            vehicle_type_id='bike',
            form_factor=FormFactor.bicycle,
            propulsion_type=PropulsionType.human,

        )]
        return types

    async def get_stations(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station(
                station_id=str(location['id']),
                name=location['name'],
                lat=location['latitude'],
                lon=location['longitude'],
                address=location['street'],
                capacity=location['numberOfLocks'],
            ))
        return stations

    async def get_station_status(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station1(
                station_id=str(location['id']),
                num_bikes_available=location['numberOfFreeBikes'],
                num_docks_available=location['numberOfFreeLocks'],
                vehicle_types_available=[
                    VehicleTypesAvailableItem(
                        vehicle_type_id='bike',
                        count=location['numberOfFreeBikes'],
                    )
                ],
                is_installed=True,
                is_renting=True,
                is_returning=True,
                num_docks_disabled=0,  # TODO: confirm the meaning of 'numberOfTotalFaulty'
                last_reported=int(time.time()),  # TODO: last report time
            ))
        return stations
