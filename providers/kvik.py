import json
from typing import List

import aiohttp as aiohttp
from pydantic import AnyUrl

from gbfs_model.station_information import Station, Type, StationArea
from gbfs_model.free_bike_status import Bike
from gbfs_model.vehicle_types import VehicleType, PropulsionType, FormFactor
from provider import GBFSProvider


DEFAULT_VEHICLE_TYPE = "1"

IMG_DUAL_WHEEL: AnyUrl = "https://assets-global.website-files.com/64bf715f5d5520c9b7a6d9fa/64bf95348945f482125123c1_about.png"  # noqa
IMG_SINGLE_WHEEL: AnyUrl = "https://assets-global.website-files.com/64bf715f5d5520c9b7a6d9fa/64bf9534f568407726e850ca_referal.png"  # noqa


class Provider(GBFSProvider):
    id = "kvik"
    name = "Kvik"
    supported_feeds = ('system_information', 'free_bike_status', 'vehicle_types', 'station_information')

    async def get_free_bike_status(self):
        url = "https://kvik-scooter.com/get-scooters"
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()

        stations = []
        for location in data:
            stations.append(Bike(
                bike_id=str(location['id']),
                lat=location['latitude'],
                lon=location['longitude'],
                is_reserved=False,
                is_disabled=False,
                vehicle_type_id=DEFAULT_VEHICLE_TYPE,
                current_range_meters=location.get('lengthCalculatedMeters'),  # TODO: is this really what that means?
            ))
        return stations

    async def get_vehicle_types(self):
        # TODO: is there a way to tell between signle and dual wheel scooters?
        return [
            VehicleType(
                vehicle_type_id=DEFAULT_VEHICLE_TYPE,
                form_factor=FormFactor.scooter_standing,
                propulsion_type=PropulsionType.electric,
                name="Electric scooter",
                max_range_meters=60_000,
                vehicle_image=IMG_SINGLE_WHEEL,
            )
        ]

    async def get_stations(self):  # noqa - "method could be static" is a dumb linter rule
        url = "https://kvik-scooter.com/ride-borders"
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()
        station_borders = [b for b in data if b['type'] == 'ParkingLot']

        stations = []
        for location in station_borders:
            border: List[List[float]] = json.loads(location['border'])
            # TODO: better station ID?
            st_id = str(abs(hash(location['border'])))

            # GeoJSON spec: "The first and last points in the linear ring should be identical"
            if border[0] != border[-1]:
                border.append(border[0])

            center_coords = poly_center(border)

            stations.append(Station(
                station_id=st_id,
                # TODO: stations don't have names, but GBFS requires them
                name=st_id,
                lat=center_coords[1],
                lon=center_coords[0],
                is_virtual_station=True,
                station_area=StationArea(
                    type=Type.MultiPolygon,
                    coordinates=[[border]]  # noqa - type checker is confused, this is correct
                ),
            ))
        return stations


def poly_center(verts: List[List[float]]):
    """Calculate the center of a given polygon, defined by its vertices"""
    centroid_x = sum(vert[0] for vert in verts) / len(verts)
    centroid_y = sum(vert[1] for vert in verts) / len(verts)

    return [centroid_x, centroid_y]
