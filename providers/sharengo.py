import logging
from typing import Dict, override
import aiohttp as aiohttp

from gbfs_model.station_information import Station
from gbfs_model.free_bike_status import Bike, RentalUris
from gbfs_model.system_information import Android, Ios, RentalApps
from gbfs_model.vehicle_types import VehicleType, PropulsionType, FormFactor
from provider import GBFSProvider


PROVIDER_ID = "58ee0cc36d818563a9ff46af"


class Provider(GBFSProvider):
    id = "sharengo"
    name = "SHARE'Ngo"
    supported_feeds = ('system_information', 'free_bike_status', 'vehicle_types', )

    async def _fetch_locations(self):
        url = "https://sharengo.core.gourban.services/front/vehicles"
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()
        return data

    @override
    def get_system_info(self):
        info = super().get_system_info()
        info.rental_apps = RentalApps(
            android=Android(
                store_uri='https://play.google.com/store/apps/details?id=si.sharengo.sharengoapp',
                discovery_uri='si.sharengo.sharengoapp://',
            ),
            ios=Ios(
                store_uri='https://apps.apple.com/si/app/sharengo/id1577919169',
                discovery_uri='si.sharengo.sharengoapp://'
            ),
        )
        return info

    async def get_free_bike_status(self):
        cars = await self._fetch_locations()
        
        stations = []
        for location in cars:
            # Skip test car
            if ' ' in location['code']:
                continue

            stations.append(Bike(
                bike_id=str(location['id']),
                lat=location['position']['coordinates'][1],
                lon=location['position']['coordinates'][0],
                is_reserved=False,
                is_disabled=False,
                vehicle_type_id=str(location['categoryId']),
                current_range_meters=location.get('remainingKilometers'),
                current_fuel_percent=location.get('stateOfCharge'),
                rental_uris=RentalUris(
                    android=f'https://go.gourban.co/sharengo/map?vehicleCode={location["code"]}',
                    web=f'https://go.gourban.co/sharengo/map?vehicleCode={location["code"]}',
                )
            ))
        return stations

    async def _get_vehicle_types(self):
        url = 'https://platform.api.gourban.services/v1/sharengo/front/vehicles/categories'
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()
        
        types: Dict[int, VehicleType] = {}
        for type in data:
            if not type['bookable']: 
                continue
            types[type['id']] = VehicleType(
                vehicle_type_id=str(type['id']),
                form_factor=FormFactor.car,
                # Apparently there's one "MG ZS GAS"
                propulsion_type=PropulsionType.electric if not "GAS" in type['name'] else PropulsionType.combustion,
                name=type['name'],
                vehicle_image=type['appProperties'].get('image', type['sharedProperties'].get('image')),
            )
        return types

    async def get_vehicle_types(self) -> list[VehicleType]:
        return list((await self._get_vehicle_types()).values())
