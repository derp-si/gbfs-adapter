from provider import GBFSProvider

FLEET_MAP = {
    544: 'GreenGo',
    1451: 'Škofja loka',
}

class Provider(GBFSProvider):
    id = "greengo"
    name = "GreenGo (meta-provider)"
    supported_feeds = ()
    
    fleet_id: int

    def __init__(self, fleet__id):
        self.id = 'greengo_' + str(fleet__id)
        self.name = 'GreenGo ' + FLEET_MAP[fleet__id]
        self.fleet_id = fleet__id

    @classmethod
    def create_providers(cls):
        return [cls(fleet_id) for fleet_id in FLEET_MAP.keys()]

    def get_url(self, base_url='', path='gbfs.json'):
        path = path.replace('.json', '')
        return f'https://greengo.rideatom.com/gbfs/v2_2/en/{path}?id={self.fleet_id}'
