import json
import time
import urllib.request
from datetime import datetime

import aiohttp as aiohttp
from dateutil.parser import isoparse

from gbfs_model.station_information import Station
from gbfs_model.station_status import Station as Station1, VehicleTypesAvailableItem
from gbfs_model.vehicle_types import VehicleType, FormFactor, PropulsionType
from provider import GBFSProvider


API_KEY = 'frifk0jbxfefqqniqez09tw4jvk37wyf823b5j1i'


class Provider(GBFSProvider):
    id = "jcdecaux"
    name = "JCDecaux"
    supported_feeds = ('system_information', 'station_information', 'station_status', 'vehicle_types',)

    contract = None

    def __init__(self, system_info):
        self.contract = system_info['name']
        self.id = 'jcdecaux_' + system_info['name']
        self.name = system_info['commercial_name'] if system_info['commercial_name'] else system_info['name'].title()

    @classmethod
    def create_providers(cls):
        obj = urllib.request.urlopen('https://api.jcdecaux.com/vls/v1/contracts?apiKey=' + API_KEY)
        jsonobj = obj.read().decode('utf-8')
        data = json.loads(jsonobj)
        return [cls(system) for system in data if system['country_code'] == "SI" or system['name'] == 'maribor']

    async def _fetch_locations(self):
        url = "https://api.jcdecaux.com/vls/v3/stations?contract=" + self.contract + "&apiKey=" + API_KEY
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            data = await resp.json()
        return data

    async def get_vehicle_types(self):
        types = [
            VehicleType(
                vehicle_type_id='bike',
                form_factor=FormFactor.bicycle,
                propulsion_type=PropulsionType.human,

            ),
            # VehicleType(
            #     vehicle_type_id='electric_bike',
            #     form_factor=FormFactor.bicycle,
            #     propulsion_type=PropulsionType.electric_assist,
            #     max_range_meters=0,  # TODO: e-bike range
            # )
        ]
        return types

    async def get_stations(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station(
                station_id=str(location['number']),
                name=location['name'],
                lat=location['position']['latitude'],
                lon=location['position']['longitude'],
                address=location['address'],
                capacity=location['totalStands']['capacity'],
            ))
        return stations

    async def get_station_status(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station1(
                station_id=str(location['number']),
                num_bikes_available=location['totalStands']['availabilities']['bikes'],
                num_docks_available=location['totalStands']['availabilities']['stands'],
                vehicle_types_available=[
                    VehicleTypesAvailableItem(
                        vehicle_type_id='bike',
                        count=location['totalStands']['availabilities']['mechanicalBikes'],
                    ),
                    # VehicleTypesAvailableItem(
                    #     vehicle_type_id='electric_bike',
                    #     count=location['totalStands']['availabilities']['electricalBikes'],
                    # )
                ],
                is_installed=True,
                is_renting=location['status'] == 'OPEN',
                is_returning=location['status'] == 'OPEN',
                num_docks_disabled=0,
                last_reported=int(isoparse(location['lastUpdate']).timestamp()) if location['lastUpdate'] else int(time.time()),
            ))
        return stations

