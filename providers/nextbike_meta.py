import json
import urllib.request

from provider import GBFSProvider


class Provider(GBFSProvider):
    id = "nextbike"
    name = "Nextbike (meta-provider)"
    supported_feeds = ()

    def __init__(self, system_info):
        self.system_info = system_info
        self.id = 'nextbike_' + system_info['domain']
        self.name = system_info['name']

    @classmethod
    def create_providers(cls):
        obj = urllib.request.urlopen('https://api.nextbike.net/maps/nextbike-live.json')
        jsonobj = obj.read().decode('utf-8')
        data = json.loads(jsonobj)
        return [cls(system) for system in data['countries'] if system['country'] == 'SI']

    def get_url(self, base_url='', path='gbfs.json'):
        return 'https://api.nextbike.net/maps/gbfs/v2/' + self.id + '/' + path