import json
import time
import urllib.request
import aiohttp as aiohttp
from asyncio.locks import Lock

from gbfs_model.station_information import Station
from gbfs_model.station_status import Station as Station1, VehicleTypesAvailableItem
from gbfs_model.vehicle_types import VehicleType, FormFactor, PropulsionType
from provider import GBFSProvider


class Provider(GBFSProvider):
    id = "micikel"
    name = "MICikel"
    supported_feeds = ('system_information', 'station_information', 'station_status', 'vehicle_types',)

    # Multi-system provider is currently disabled since they don't provide enough data to differentiate between systems

    # def __init__(self, system_info):
    #     self.system_info = system_info
    #     self.id = 'micikel_' + system_info['sistemId']
    #     self.name = system_info['sistem']

    # @classmethod
    # def create_providers(cls):
    #     obj = urllib.request.urlopen('https://api.micikel.bike/sistemi')
    #     jsonobj = obj.read().decode('utf-8')
    #     data = json.loads(jsonobj)
    #     return [cls(system) for system in data]

    async def _fetch_locations(self):
        # Mobile app API doesn't differentiate between systems or between e-bikes and normal ones
        # so we're stuck scraping the JS-array-in-HTML on the web
        url = "https://prijava.micikel.bike/zemljevid/"
        async with aiohttp.request("GET", url) as resp:
            resp.raise_for_status()
            html = await resp.text()

        array = html.split("locations = ", maxsplit=1)[1].split(";", maxsplit=1)[0]
        array = array.replace("'", "\"").replace(",]", "]")
        data = json.loads(array)

        bikes = []
        for location in data:
            # Field mapping is copied from JS source
            bikes.append({
                'id_im': location[0],
                'naziv_im': location[1],
                'naslovim': location[2],
                'lat': location[3],
                'lang': location[4],
                'izposoja_izdaja': location[5],
                'skupaj': location[6],
                'prostih': location[7],
                'st_klasicnih': location[8],
                'skupaj_elektricnih': location[9],
                'prostih_elektricnih': location[10],
                'st_elektricnih': location[11],
            })

        return bikes

    async def get_vehicle_types(self):
        types = [VehicleType(
            vehicle_type_id='bike',
            form_factor=FormFactor.bicycle,
            propulsion_type=PropulsionType.human,
        ), VehicleType(
            vehicle_type_id='e_bike',
            form_factor=FormFactor.bicycle,
            propulsion_type=PropulsionType.electric_assist,
        )]
        return types

    async def get_stations(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station(
                station_id=str(location['id_im']),
                name=location['naziv_im'],
                lat=location['lat'],
                lon=location['lang'],
                address=location['naslovim'],
                capacity=location['skupaj'],
            ))
        return stations

    async def get_station_status(self):
        locations = await self._fetch_locations()
        stations = []
        for location in locations:
            stations.append(Station1(
                station_id=str(location['id_im']),
                num_bikes_available=location['st_elektricnih'] + location['st_klasicnih'],
                # There's something weird about these numbers, see the HTML for their strange workaround
                # TODO: contact Micikel for clarification about this
                num_docks_available=location['prostih'] or location['prostih_elektricnih'],
                vehicle_types_available=[
                    VehicleTypesAvailableItem(
                        vehicle_type_id='bike',
                        count=location['st_klasicnih'],
                    ),
                    VehicleTypesAvailableItem(
                        vehicle_type_id='e_bike',
                        count=location['st_elektricnih'],
                    )
                ],
                is_installed=True,
                is_renting=True,
                is_returning=True,
                num_docks_disabled=0,
                last_reported=int(time.time()),
            ))
        return stations
