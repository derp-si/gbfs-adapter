"""
Cursed wrapper around datamodel-codegen to path the type mapping from uri->AnyUrl to uri->str

This is necessary because of this bug:
    https://github.com/koxudaxi/datamodel-code-generator/issues/2186
"""
import sys
from datamodel_code_generator.model.pydantic import types
from datamodel_code_generator.types import Types

original_func = types.type_map_factory

def patched_type_map_factory(*args, **kwargs):
    map = original_func(*args, **kwargs)
    map[Types.uri] = map[Types.string]
    return map

types.type_map_factory = patched_type_map_factory

from datamodel_code_generator.__main__ import main

sys.exit(main())
