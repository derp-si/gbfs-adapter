import time

from fastapi import APIRouter, Depends, Request
from fastapi.exceptions import HTTPException

from config import PROVIDER_MAP
from gbfs_model import gbfs, gbfs_versions, system_information, vehicle_types, station_information, station_status, free_bike_status
from gbfs_model.gbfs_versions import Version1
from provider import GBFSProvider

router = APIRouter(
    prefix="/gbfs",
)

VERSION: str = Version1.field_2_3.value


def get_url(feed_name, provider, request: Request):
    base_url = request.url.scheme + '://' + request.url.netloc
    return base_url + router.url_path_for('_' + feed_name, provider=provider.id)


def provider_dependency(provider: str) -> GBFSProvider:
    try:
        return PROVIDER_MAP[provider]
    except KeyError:
        raise HTTPException(status_code=404, detail="This provider does not exist")


def wrap(module, provider, data):
    return getattr(module, 'Model')(
        last_updated=int(time.time()),
        ttl=0,
        version=str(VERSION),
        data=data
    )


@router.get('/{provider}/gbfs.json',
            description='Auto-discovery file that links to all of the other files published by the system.',
            )
async def _gbfs(request: Request, provider=Depends(provider_dependency)):
    feeds = [
        gbfs.Feed(
            url=get_url(feed_name, provider, request),
            name=feed_name
        ) for feed_name in provider.supported_feeds
    ]
    return wrap(gbfs, provider, {
        provider.language: gbfs.Data(
            feeds=feeds,
        )
    })


@router.get('/{provider}/gbfs_versions.json',
            description='Lists all feed endpoints published according to versions of the GBFS documentation.'
            )
async def _gbfs_versions(request: Request, provider=Depends(provider_dependency)):
    return wrap(gbfs_versions, provider, gbfs_versions.Data(
        versions=[
            gbfs_versions.Version(
                version=VERSION,
                url=get_url('gbfs', provider=provider, request=request),
            )
        ]
    ))


# REQUIRED of systems that include information about vehicle types in the free_bike_status.json file.
# If this file is not included, then all vehicles in the feed are assumed to be non-motorized bicycles.
@router.get('/{provider}/system_information.json',
            description='Details including system operator, system location, year implemented, URL, contact info, time zone.'
            )
async def _system_information(provider=Depends(provider_dependency)):
    return wrap(system_information, provider, provider.get_system_info())


@router.get('/{provider}/vehicle_types.json',
            description='Describes the types of vehicles that system operator has available for rent'
            )
async def _vehicle_types(provider=Depends(provider_dependency)):
    return wrap(vehicle_types, provider, vehicle_types.Data(
        vehicle_types=await provider.get_vehicle_types()
    ))


# REQUIRED of systems utilizing docks
@router.get('/{provider}/station_information.json',
            description='Conditionally REQUIRED 	List of all stations, their capacities and locations.'
            )
async def _station_information(provider=Depends(provider_dependency)):
    return wrap(station_information, provider, station_information.Data(
        stations=await provider.get_stations()
    ))


# REQUIRED of systems utilizing docks.
@router.get('/{provider}/station_status.json',
            description='Number of available vehicles and docks at each station and station availability.'
            )
async def _station_status(provider=Depends(provider_dependency)):
    return wrap(station_status, provider, station_status.Data(
        stations=await provider.get_station_status()
    ))


# REQUIRED for free floating (dockless) vehicles. or station based (docked) vehicles. Vehicles that are part of an active rental MUST NOT appear in this feed.
@router.get('/{provider}/free_bike_status.json',
            description='Describes all vehicles that are not currently in active rental. '
            )
async def _free_bike_status(provider=Depends(provider_dependency)):
    return wrap(free_bike_status, provider, free_bike_status.Data(
        bikes=await provider.get_free_bike_status()
    ))


# OPTIONAL
@router.get('/{provider}/system_hours.json',
            description='Hours of operation for the system.'
            )
async def _system_hours(provider=Depends(provider_dependency)):
    return wrap(provider, None)


# OPTIONAL
@router.get('/{provider}/system_calendar.json',
            description='Dates of operation for the system.'
            )
async def _system_calendar(provider=Depends(provider_dependency)):
    return wrap(provider, None)


# OPTIONAL
@router.get('/{provider}/system_regions.json',
            description='Regions the system is broken up into.'
            )
async def _system_regions(provider=Depends(provider_dependency)):
    return wrap(provider, None)


# OPTIONAL
@router.get('/{provider}/system_pricing_plans.json',
            description='System pricing scheme.'
            )
async def _system_pricing_plans(provider=Depends(provider_dependency)):
    return wrap(provider, None)


# OPTIONAL
@router.get('/{provider}/system_alerts.json',
            description='Current system alerts.'
            )
async def _system_alerts(provider=Depends(provider_dependency)):
    return wrap(provider, None)


# OPTIONAL
@router.get('/{provider}/geofencing_zones.json',
            description='Geofencing zones and their associated rules and attributes.'
            )
async def _geofencing_zones(provider=Depends(provider_dependency)):
    return wrap(provider, None)
