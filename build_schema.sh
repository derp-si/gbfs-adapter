#!/bin/sh
VERSION="v2.3"
SCHEMA_DIR="gbfs-json-schema/$VERSION"
set -e

#
# Fix mistake in schema
#
echo Fixing schema...
jq <"$SCHEMA_DIR/free_bike_status.json" 'walk(if type == "object" then del(.anyOf) else . end)' > "$SCHEMA_DIR/free_bike_status.json.tmp"
mv "$SCHEMA_DIR/free_bike_status.json.tmp" "$SCHEMA_DIR/free_bike_status.json"

jq <"$SCHEMA_DIR/gbfs.json" 'walk(if type == "object" and .required == true then del(.required) else . end)' > "$SCHEMA_DIR/gbfs.json.tmp"
mv "$SCHEMA_DIR/gbfs.json.tmp" "$SCHEMA_DIR/gbfs.json"

#
# Codegen
#
echo Generating models...
mkdir -p gbfs_model
rm -rf gbfs_model/*
python ./build_schema.py --input "$SCHEMA_DIR/" --output "gbfs_model" --output-model-type pydantic_v2.BaseModel --base-class models.CustomBaseModel --reuse-model --input-file-type jsonschema

#
# Fix codegen
#
echo Fixing models...
find "gbfs_model" -name '*.py' -exec sed -i 's/Data[0-9]*/Data/g' {} \;
