import pytest
import json
import os
import subprocess
import sys
from pathlib import Path
import time

import requests


REPORT_DIR = Path('./gbfs_validation')
REPORT_DIR.mkdir(exist_ok=True)

if os.environ.get('CI'):
    VALIDATOR_URL = 'http://gbfs-validator:8080'
    SELF_URL = 'http://gbfs-adapter:80'
else:
    VALIDATOR_URL = 'http://127.0.0.1:8080'
    SELF_URL = 'http://127.0.0.1:42069'



# Define the pytest_generate_tests hook to generate test cases
def pytest_generate_tests(metafunc):
    global PROVIDERS

    if 'provider' in metafunc.fixturenames:
        
        resp = requests.get(SELF_URL + '/providers')
        PROVIDERS = {p['id']: p for p in resp.json()}

        metafunc.parametrize('provider', PROVIDERS.keys())


def validate_gbfs(provider: dict) -> dict:
    resp = requests.post(VALIDATOR_URL + '/.netlify/functions/validator', json={'url': provider['url']})
    resp.raise_for_status()
    report = resp.json()

    with open(REPORT_DIR / f"{provider['id']}.json", 'w') as f:
        json.dump(report, f, indent=4)
    
    if 'CI' in os.environ:
        print(f"[[ATTACHMENT|{REPORT_DIR.name}/{provider['id']}.json]]")

    return report

def test_provider(provider: str):
    global PROVIDERS
    validation = validate_gbfs(PROVIDERS[provider])
    
    if 'gbfsResult' in validation['summary']:
        if not validation['summary']['gbfsResult']['exists']:
            pytest.fail("GBFS endpoint not reachable: " + validation['summary']['gbfsResult']['url'], pytrace=False)
    
    if validation['summary']['hasErrors']:
        # Print out the errors so they show in the report
        for file in validation['files']:
            if file['hasErrors']:
                print(file['file'])
                errors = file['languages'][0]
                for error in errors['errors']:
                    print("\t", error['instancePath'], error['message'])
        # Fail the test
        pytest.fail(f"GBFS validation for '{provider}' failed. See report for details: {REPORT_DIR/provider}.json", pytrace=False)