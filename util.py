from typing import Tuple, Optional

from fastapi_cache.backends import Backend


class DummyCacheBackend(Backend):

    async def get_with_ttl(self, key: str) -> Tuple[int, Optional[str]]:
        return 0, None

    async def get(self, key: str) -> str:
        pass

    async def set(self, key: str, value: str, expire: int = None):
        pass

    async def clear(self, namespace: str = None, key: str = None) -> int:
        return 0
