import os

ENVIRONMENT = os.getenv('ENVIRONMENT', 'dev-unknown')

PROVIDER_NAMES = ('avant2go', 'sharengo', 'scbikes', 'jcdecaux', 'nextbike_meta', 'kvik', 'greengo_meta', 'micikel',)

if os.getenv('PROVIDERS'):
    PROVIDER_NAMES = os.getenv('PROVIDERS').split(',')

USE_CACHE = not ENVIRONMENT.startswith('dev')

# Shared state, stored in a global variable in the config module? Scandalous!
PROVIDER_MAP = {}

PORT = os.getenv('PORT', '42069')

USE_SENTRY = os.getenv('SENTRY_DSN') is not None and 'prod' in ENVIRONMENT
SENTRY_DSN = os.getenv('SENTRY_DSN')